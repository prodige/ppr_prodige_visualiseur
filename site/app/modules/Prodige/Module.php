<?php

namespace Visualiseur\Prodige;

use Phalcon\Config;
use Phalcon\Di\DiInterface;
use Phalcon\Mvc\ModuleDefinitionInterface;

/**
 * Class Module
 * Setup Visualiseur Prodige module
 *
 * @package Visualiseur\Prodige
 */
class Module implements ModuleDefinitionInterface
{
    /**
     * Module base directory
     */
    const PATH = __DIR__;
    /**
     * Registers the module auto-loader
     *
     * @param DiInterface|null $di
     */
    public function registerAutoloaders(DiInterface $di = null)
    {
    }
    /**
     * Registers the module-only services
     *
     * @param DiInterface $di
     */
    public function registerServices(DiInterface $di)
    {
        $di->getShared('config')->merge(new Config(require_once self::PATH . "/config/config.php"));
        require_once self::PATH . "/config/services.php";
    }
}