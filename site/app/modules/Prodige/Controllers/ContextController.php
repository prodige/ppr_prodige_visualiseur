<?php

namespace Visualiseur\Prodige\Controllers;

use Visualiseur\Core\Controllers\Traits\HttpTrait;
use Visualiseur\Core\Controllers\Traits\RequestTrait;
use Visualiseur\Prodige\Controllers\Traits\UserTrait;
use Visualiseur\Core\Controllers\Traits\ContextTrait;
use Visualiseur\Prodige\Controllers\Traits\ProdigeContextTrait;
use Visualiseur\Prodige\Models\Context;
use Visualiseur\Prodige\Models\User;

/**
 * Class ContextController
 *
 * @package Visualiseur\Prodige\Controllers
 */
class ContextController extends ControllerBase
{

    use HttpTrait;
    use RequestTrait;
    use UserTrait;
    use ContextTrait;
    use ProdigeContextTrait;

    public function indexAction()
    {
        $this->getContext();
    }

    public function saveContextAction()
    {
        $response = $this->saveContext();
        exit();
    }

    public function listAction()
    {
        $response = new \Phalcon\Http\Response();
        try {
            $checkUser = $this->checkCurrentUser();
            $userName = $this->getCurrentUser()->getUserName();
            $userId = $this->getCurrentUser()->getId();

            $contexts = Context::find(["idUser = :IDUSER:", "bind" => ['IDUSER' => $userId]]);
            $result = array();

            foreach ($contexts as $context) {
                $arrayContext = json_decode($context->context, true);
                if (!is_null($arrayContext) && array_key_exists("properties", $arrayContext) && array_key_exists("title", $arrayContext["properties"])) {
                    array_push($result, array("id" => $context->id, "title" => $arrayContext["properties"]["title"])); //, "context" => $context->context
                }
            }

            $titleSorterFunction = function($a, $b) {
                return strcmp(strtolower($a["title"]), strtolower($b["title"]));
            };
            usort($result, $titleSorterFunction);

            $response->setJsonContent([
                "success" => true,
                "userName" => $userName,
                "message" => "",
                "data" =>
                $result,
            ]);
            return $response;
        } catch (\Exception $e) {
//            throw $e; //for dev
            $response->setJsonContent([
                "success" => false,
                "message" => "Failed to list context. " . $e->getMessage(),
            ]);
            return $response;
        }
    }



    public function saveAction()
    {
        $response = new \Phalcon\Http\Response();
        $userName = "";
        try {
            // if checkCurrentUser is uncommented, then request is returning as get so failing
            $checkUser = $this->checkCurrentUser();

            $userName = $this->getCurrentUser()->getUserName();
            $userId = $this->getCurrentUser()->getId();

            $anonymousName = $this->config->params->PRO_USER_INTERNET_USR_ID;

            if (!$this->request->isPost()) {
                $response->setJsonContent([
                    "success" => false,
                    "message" => "Post Request is mandatory.",
                ]);
                return $response;
            }

            if (strcmp($userName, $anonymousName) == 0) {
//                throw new \Exception("Failed to identified current user."); //for dev

                $response->setStatusCode(401);
                $response->setContent("Failed to identified current user.");
                $response->send();
                return $response;

                // PHP natif
//                header("HTTP/1.0 401 Unauthorized");
//                return null;
            }

            $json = json_decode($this->request->getRawBody(), true);

            if (is_null($json)) {
                throw new \Exception("Failed to parse JSON. Content length is " . strlen($this->request->getRawBody()));
            }

            if (!isset($json["context"])) {
                throw new \Exception("parameter context is missing.");
            }
            if (!isset($json["context"]["properties"]["title"])) {
                throw new \Exception("parameter title is missing.");
            }

            $context = $json["context"];
            $title = $json["context"]["properties"]["title"];
            $jsonContext = json_encode($context);

            $newContext = new Context();
            $newContext->idUser = $userId;
            $newContext->context = $jsonContext;
            $newContext->save();

            $response->setJsonContent([
                "success" => true,
                "userName" => $userName,
                "message" => "",
                "data" => [
                    "id" => $newContext->id,
                    "title" => $title,
                    "context" => $jsonContext,
                ]
            ]);
            return $response;
        } catch (\Exception $e) {
//            throw $e; //for dev
            $response->setJsonContent([
                "success" => false,
                "userName" => $userName,
                "message" => "Failed to save context. " . $e->getMessage(),
            ]);
            return $response;
        }
    }

    public function getAction()
    {
        $response = new \Phalcon\Http\Response();
        try {

            $contextId = $this->request->getQuery("id");
            if (is_null($contextId)) {
                throw new \Exception("id is missing.");
            }
            if (!is_numeric($contextId)) {
                throw new \Exception("id is invalid.");
            }

            $checkUser = $this->checkCurrentUser();
            $userName = $this->getCurrentUser()->getUserName();
            $userId = $this->getCurrentUser()->getId();

            $contexts = Context::find(["idUser = :IDUSER: and id = :IDCONTEXT:", "bind" => ['IDUSER' => $userId, 'IDCONTEXT' => $contextId]]);
            $result = array();

            foreach ($contexts as $context) {
                $arrayContext = json_decode($context->context, true);

                if (!is_null($arrayContext) && array_key_exists("properties", $arrayContext) && array_key_exists("title", $arrayContext["properties"])) {

                    $response->setJsonContent([
                        "success" => true,
                        "userName" => $userName,
                        "message" => "",
                        "data" => $arrayContext
                    ]);
                    return $response;
                }
            }

            throw new \Exception("context not found.");
        } catch (\Exception $e) {
//            throw $e; //for dev
            $response->setJsonContent([
                "success" => false,
                "message" => "Failed to get context. " . $e->getMessage(),
            ]);
            return $response;
        }
    }

    public function renameAction()
    {
        $response = new \Phalcon\Http\Response();
        try {

            $contextId = $this->request->getQuery("id");
            if (is_null($contextId)) {
                throw new \Exception("id is missing.");
            }
            if (!is_numeric($contextId)) {
                throw new \Exception("id is invalid.");
            }
            $title = $this->request->getQuery("title");
            if (is_null($title)) {
                throw new \Exception("title is missing.");
            }

            $checkUser = $this->checkCurrentUser();
            $userName = $this->getCurrentUser()->getUserName();
            $userId = $this->getCurrentUser()->getId();

            $contexts = Context::find(["idUser = :IDUSER: and id = :IDCONTEXT:", "bind" => ['IDUSER' => $userId, 'IDCONTEXT' => $contextId]]);

            foreach ($contexts as $context) {
                $arrayContext = json_decode($context->context, true);
                if (!is_null($arrayContext) && array_key_exists("properties", $arrayContext) && array_key_exists("title", $arrayContext["properties"])) {
                    $arrayContext["properties"]["title"] = $title;
                    $jsonContext = json_encode($arrayContext);
                    $context->context = $jsonContext;
                    $context->save();

                    $response->setJsonContent([
                        "success" => true,
                        "message" => "",
                        "userName" => $userName,
                    ]);
                    return $response;
                }
            }

            throw new \Exception("context not found.");
        } catch (\Exception $e) {
//            throw $e; //for dev
            $response->setJsonContent([
                "success" => false,
                "message" => "Failed to rename context. " . $e->getMessage(),
            ]);
            return $response;
        }
    }

    public function deleteAction()
    {
        $response = new \Phalcon\Http\Response();
        try {

            $contextId = $this->request->getQuery("id");
            if (is_null($contextId)) {
                throw new \Exception("id is missing.");
            }
            if (!is_numeric($contextId)) {
                throw new \Exception("id is invalid.");
            }

            $checkUser = $this->checkCurrentUser();
            $userName = $this->getCurrentUser()->getUserName();
            $userId = $this->getCurrentUser()->getId();

            $contexts = Context::find(["idUser = :IDUSER: and id = :IDCONTEXT:", "bind" => ['IDUSER' => $userId, 'IDCONTEXT' => $contextId]]);

            foreach ($contexts as $context) {
                $context->delete();

                $response->setJsonContent([
                    "success" => true,
                    "message" => "",
                    "userName" => $userName,
                ]);
                return $response;
            }

            throw new \Exception("context not found.");
        } catch (\Exception $e) {
//            throw $e; //for dev
            $response->setJsonContent([
                "success" => false,
                "message" => "Failed to delete context. " . $e->getMessage(),
            ]);
            return $response;
        }
    }

}
