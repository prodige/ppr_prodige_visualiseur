<?php

namespace Visualiseur\Prodige\Controllers\Traits;

/**
 * ContextTrait -- specific methods for PRODIGE
 */
Trait ProdigeContextTrait
{
  
    /**
     * Surcharge de geobookmarkConfig avec les paramètres BD
     * @param array $context json Context
     * @return json context
     */
    protected function updateLocatorParameters($context){

        if($context && $context["properties"]["extension"]["Tools"]["Localisation"] && empty($context["properties"]["extension"]["Tools"]["LocalisationConfig"])){
            
            //get params from database
            $connection = new \Phalcon\Db\Adapter\Pdo\Postgresql(
                [
                    'host'      => $this->config->params->prodige_host,
                    'username'  => $this->config->params->prodige_user,
                    'port'      => $this->config->params->prodige_port,
                    'password'  => $this->config->params->prodige_password,
                    'dbname'   =>  $this->config->params->prodige_name
                ]
            );

            $sql = "select * from parametrage.prodige_search_param where critere_moteur_nom <>'' and critere_moteur_nom is not null order by pk_critere_moteur";

            $result = $connection->query(
                $sql
            );
           
            while ($criteria = $result->fetch()) {
                $localisationCriteria = new \StdClass();
                $localisationCriteria->title = $criteria["critere_moteur_nom"];
                $localisationCriteria->layerId = "layer".$criteria["pk_critere_moteur"];
                $localisationCriteria->layerCodeField = $criteria["critere_moteur_champ_id"];
                $localisationCriteria->layerWFSUrl = $this->config->params->PRODIGE_URL_DATACARTO."/map/locator?typename="."layer".$criteria["pk_critere_moteur"]."_locator";
                $localisationCriteria->layerTextField = $criteria["critere_moteur_champ_nom"];
                $localisationCriteria->criteriaRelated = ($criteria["critere_moteur_id_join"] == 0 || $criteria["critere_moteur_id_join"] == ""   ? "" :  $criteria["critere_moteur_id_join"]);
                $localisationCriteria->criteriaFieldRelated = $criteria["critere_moteur_champ_join"];
                $localisationCriteria->criteriaInSearch = false;
                $context["properties"]["extension"]["Tools"]["LocalisationConfig"][]= $localisationCriteria;
                //"LocalisationConfig":[{"title":"R\u00e9gion","layerId":"layer3","layerCodeField":"num_reg","layerWFSUrl":
                    //"https:\/\/datacarto-prodige41.alkante.al:19020\/map\/\/ars_metropole_udi_infofactures?typename=layer3_locator",
                    //"layerTextField":"region","criteriaRelated":"","criteriaFieldRelated":"","criteriaInSearch":false},{"title":"D\u00e9partement","layerId":"layer4","layerCodeField":"code_dept","layerWFSUrl":"https:\/\/datacarto-prodige41.alkante.al:19020\/map\/\/ars_metropole_udi_infofactures?typename=layer4_locator","layerTextField":"nom_dept","criteriaRelated":1,"criteriaFieldRelated":"code_reg","criteriaInSearch":false},{"title":"Commune","layerId":"layer7","layerCodeField":"insee_com","layerWFSUrl":"https:\/\/datacarto-prodige41.alkante.al:19020\/map\/\/ars_metropole_udi_infofactures?typename=layer7_locator","layerTextField":"nom_com","criteriaRelated":2,"criteriaFieldRelated":"code_dept","criteriaInSearch":false}],
            }
            

        }
        return $context;

    }

    protected function loadSettingsProdige(){
        //get params from database
        $connection = new \Phalcon\Db\Adapter\Pdo\Postgresql(
            [
                'host'      => $this->config->params->prodige_host,
                'username'  => $this->config->params->prodige_user,
                'port'      => $this->config->params->prodige_port,
                'password'  => $this->config->params->prodige_password,
                'dbname'   =>  $this->config->params->prodige_name
            ]
        );

        $sql = 'SELECT prodige_settings_constant, prodige_settings_value from parametrage.prodige_settings';

        $result = $connection->query(
            $sql
        );

        $settings = array();
        while ($row = $result->fetch()) {
            $settings[$row["prodige_settings_constant"]] = $row['prodige_settings_value'];
        }
        return $settings;

    }
    
    /**
     * Surcharge de tools selon les droits de l'utilisateur
     * @param array $context json Context
     * @return json context
     */
    protected function updateTools($context){

        $settings = $this->loadSettingsProdige();
      
        if($context && $context["properties"]["extension"]["Tools"]){
            
            if(isset($settings["PRO_REQUETEUR_PARADRESSE"]) && $settings["PRO_REQUETEUR_PARADRESSE"]!=""){
                $context["properties"]["extension"]["Tools"]["SearchAdressOpenLS"] = $settings["PRO_REQUETEUR_PARADRESSE"];
            }

            $curl = curl_init();
            if( !is_resource($curl) ) {
                throw new \Exception("Unable to init curl connexion : ", 500);
            }  
            
            $layers = $context["properties"]["extension"]["layers"]["layers"];
            $tabMetadataUuid = array();
            
            $this->searchMetadataUuidInLayers($layers, $tabMetadataUuid);
            
            $url = $this->config->params->PRODIGE_URL_CATALOGUE."/prodige/verify_rights_multiple";

            $data["TRAITEMENTS"]="EDITION|PUBLIPOSTAGE|NAVIGATION";
            $data["OBJET_TYPE"]="dataset";
            $data["metadatas"] = array();

            foreach($tabMetadataUuid as $uuid => $layerName){
                $metadata = new \StdClass();
                $metadata->UUID = $uuid;
                $metadata->OBJET_TYPE = "dataset";
                $metadata->OBJET_STYPE = "vector";
                $data["metadatas"][] = json_encode($metadata);
            }
            
            
            $ticket = $this->getPGT($url);

            $url.="?ticket=".$ticket;
           
            $optEnabled =
                curl_setopt($curl, CURLOPT_URL, $url)
                && curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false)
                && curl_setopt($curl, CURLOPT_BINARYTRANSFER, true)
                && curl_setopt($curl, CURLOPT_RETURNTRANSFER, true)
                && curl_setopt($curl, CURLOPT_FRESH_CONNECT, false)
                && curl_setopt($curl, CURLOPT_HEADER, false)
                && curl_setopt($curl, CURLOPT_HEADER, false)
                && curl_setopt($curl, CURLOPT_POST, true)
                && curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data))
                && curl_setopt($curl, CURLOPT_TIMEOUT, 30)
                ;
            
            if( !$optEnabled ) {
              throw new \Exception(sprintf("Unable to parameter curl connexion : [%s] %s", curl_errno($curl), curl_error($curl)), 500);
            }
    
            $resultJson = curl_exec($curl);
            

            if(!$resultJson){
                return $context;
            }

            $rights = json_decode($resultJson, true);
            
              
            if( is_resource($curl) ) {
                curl_close($curl);
            }
            
            $tabRightsUuid = array();
            $bOneActive = false;
            $bOneFilter = false;
            $filterExtent = "";
            
            foreach($rights as $uuid => $right){
   
                $active = isset($right["EDITION"]) && ( (bool)$right["EDITION"] || (bool)$right["EDITION_AJOUT"] || (bool)$right["EDITION_MODIFICATION"]);
                //change mapfile for this layer since has filter
                if(isset($right["NAVIGATION_ATTRIBUTE_FILTER"]) && !empty ($right["NAVIGATION_ATTRIBUTE_FILTER"])){
                    if(!$bOneFilter){
                        //load Map
                        $mapfile = $this->config->params->PRODIGE_PATH_DATA."cartes/Publication/".$context["id"].".map";
                        $oMap = ms_newMapObj($mapfile);
                    }
                    $this->updateLayerFilter($right["layer_table"], $tabMetadataUuid[$uuid], $oMap, $right["NAVIGATION_ATTRIBUTE_FILTER"]);
                    $bOneFilter = true;
                }
                if(isset($right["NAVIGATION_AREA"]) && !empty ($right["NAVIGATION_AREA"])){
                    if(!$bOneFilter){
                        //load Map
                        $mapfile = $this->config->params->PRODIGE_PATH_DATA."cartes/Publication/".$context["id"].".map";
                        $oMap = ms_newMapObj($mapfile);
                    }
                    if($right["NAVIGATION_AREA"]["filter_table"]=='prodige_perimetre'){
                        $this->updateLayerGeomFilter($right["layer_table"], $tabMetadataUuid[$uuid], $oMap, $right["NAVIGATION_AREA"]);
                    }
                    //if maxbbox is not bbox, some parameter for extent has already been applied, don't change it
                    if($context["properties"]["bbox"]=== $context["properties"]["extension"]["maxBbox"]){
                        $filterExtent = $this->getFilterArea($right["NAVIGATION_AREA"], $context["crs"]["properties"]["name"]);
                    }
                    
                    $bOneFilter = true;
                }
                

                $bOneActive = $active || $bOneActive;

                if($active){

                    $edition = new \StdClass();
                    $edition->uuid = $uuid;
                    
                    $edition->edition = (bool)$right["EDITION"];
                    $edition->edition_ajout = (bool)$right["EDITION_AJOUT"];
                    $edition->edition_modification = (bool)$right["EDITION_MODIFICATION"];
                    $edition->helpText = "";//$right["couchd_help_edition_msg"];
                    $edition->type = $this->getDataType($right["layer_table"]);
                    
                    $tabRightsUuid[] = $edition;    
                }
                
                if(!isset($context["properties"]["extension"]["Tools"]["Mailing"])){
                    $mailing = new \StdClass();
                    $mailing->manageModel = $right["isAdmProdige"];
                    $mailing->active = $right["PUBLIPOSTAGE"];
                    $context["properties"]["extension"]["Tools"]["Mailing"] = $mailing;    
                }
                
            }
            $context["properties"]["extension"]["Tools"]["Edition"] = new \StdClass();
            $context["properties"]["extension"]["Tools"]["Edition"]->active = $bOneActive;
            $context["properties"]["extension"]["Tools"]["Edition"]->config = $tabRightsUuid;
            

            if($bOneFilter){
                $tmpMap = "TMP_".str_replace("/", "_", $context["id"])."_".uniqid();
                //save temp mapfile
                $oMap->save($this->config->params->PRODIGE_PATH_DATA."cartes/Publication/".$tmpMap.".map");
                //replace all occurences from mapfile
                
                $this->replaceMap($context["id"], $tmpMap, $context["properties"]["extension"]["layers"]["layers"]);
                
                $context["originalId"] = $context["id"];
                $context["id"] = str_replace($context["id"], $tmpMap, $context["id"]);      
                
                //if one layer has limited area, zoom on it
                if($filterExtent && $filterExtent!=="" && $filterExtent["xmin"]){
                    //ajout d'une tolérance pour les objets ponctuels
                    //TODO :gérer le 4326
                    if( $filterExtent["xmin"] == $filterExtent["xmax"]){
                        $filterExtent["xmin"] = $filterExtent["xmin"]-100;
                        $filterExtent["xmax"] = $filterExtent["xmax"]+100;
                    }
                    if( $filterExtent["ymin"] == $filterExtent["ymax"]){
                        $filterExtent["ymin"] = $filterExtent["ymin"]-100;
                        $filterExtent["ymax"] = $filterExtent["ymax"]+100;
                    }
                    
                    $context["properties"]["bbox"][0] = (float) $filterExtent["xmin"] ;
                    $context["properties"]["bbox"][1] = (float) $filterExtent["ymin"] ;
                    $context["properties"]["bbox"][2] = (float) $filterExtent["xmax"] ;
                    $context["properties"]["bbox"][3] = (float) $filterExtent["ymax"] ;
                    
                }
                
            }

        }
        return $context;

    }

    /**
     * replace old value by new value in specific context elements
     * loop in layers
     * @param old old value
     * @param new new value
     * @param layers layers in context
     */
    protected function replaceMap($old, $new, &$layers){
        
        foreach($layers as $key => &$layer){
           
            if (array_key_exists('class', $layer) && strcmp($layer["class"], "LayerGroup") == 0) {
                if(array_key_exists("layers", $layer)){
                    $this->replaceMap($old, $new, $layer["layers"]);
                }
            }elseif(array_key_exists('extension', $layer)){
                $layer["options"]["source"]["options"]["url"] = str_replace($old, $new, $layer["options"]["source"]["options"]["url"]);
                $layer["extension"]["legendUrl"] = str_replace($old, $new, $layer["extension"]["legendUrl"]);
                $layer["extension"]["mapName"] = str_replace($old, $new, $layer["extension"]["mapName"]);
                
            }
        }

    }

    /**
     * update layer data in mapfile
     * @param layerTable table name
     * @param layerName layer name in mapfile
     * @param oMap mapObj
     * @param filter filter according to verifrights service
     */
    protected function updateLayerFilter($layerTable, $layerName, &$oMap, $filter){

        if($oMap){
            if($oLayer =@$oMap->getLayerByName($layerName)){
                //TODO do it more safely getting info and not doing replace
                $oLayer->set("data", str_replace($layerTable, $layerTable. " where ".$filter["column"]." ='".$filter["value"]."'", $oLayer->data));
            }
        }

    }
    /**
     * update layer data in mapfile
     * @param layerTable table name
     * @param layerName layer name in mapfile
     * @param oMap mapObj
     * @param filter filter according to verifrights service
     */
    protected function updateLayerGeomFilter($layerTable, $layerName, &$oMap, $filter){

        if($oMap){
            if($oLayer =@$oMap->getLayerByName($layerName)){
                //TODO do it more safely getting info and not doing replace
                $oLayer->set("data", str_replace($layerTable, $layerTable. 
                " a inner join ".$filter["filter_table"] ." b on (a.the_geom && b.the_geom and ".
                " st_intersects(a.the_geom, b.the_geom)) where ".$filter["filter_field"].
                " in ('".implode("','", $filter["filter_values"])."') "
                , $oLayer->data));

                $oLayer->set("data", str_replace("* from","distinct a.* from"
                , $oLayer->data));
            }
        }
        
    }
    

    protected function getFilterArea($filterArea, $crs){
        
        $connection = new \Phalcon\Db\Adapter\Pdo\Postgresql(
            [
                'host'      => $this->config->params->prodige_host,
                'username'  => $this->config->params->prodige_user,
                'port'      => $this->config->params->prodige_port,
                'password'  => $this->config->params->prodige_password,
                'dbname'   =>  $this->config->params->prodige_name
            ]
        );
        
        $tabInfoCrs = explode(":", $crs);
        $mapEpsg = $tabInfoCrs[count($tabInfoCrs)-1];
        $sql = "select st_xmin(st_extent(st_transform(the_geom, ".$mapEpsg."))) as xmin, st_ymin(st_extent(st_transform(the_geom, ".$mapEpsg."))) as ymin, ".
               "st_xmax(st_extent(st_transform(the_geom, ".$mapEpsg."))) as xmax, st_ymax(st_extent(st_transform(the_geom, ".$mapEpsg."))) as ymax  from ".
               $filterArea["filter_table"].
               " where ".$filterArea["filter_field"].
               " in ('".implode("','", $filterArea["filter_values"])."') ";
        
        $result = $connection->fetchAll(
            $sql
        );
        return($result[0]);
    }


    /**
     * return postgis type for table
     * @param layerTable table name
     */
    protected function getDataType($layerTable){
        
        $connection = new \Phalcon\Db\Adapter\Pdo\Postgresql(
            [
                'host'      => $this->config->params->prodige_host,
                'username'  => $this->config->params->prodige_user,
                'port'      => $this->config->params->prodige_port,
                'password'  => $this->config->params->prodige_password,
                'dbname'   =>  $this->config->params->prodige_name
            ]
        );
        $tabInfoTables = explode(".", $layerTable);
        $schemaName = (isset($tabInfoTables[1]) ? $tabInfoTables[0] : "public");
        $tableName = (isset($tabInfoTables[1]) ? $tabInfoTables[1] : $tabInfoTables[0]);

        $sql = "SELECT type from public.geometry_columns where f_table_schema=? and f_table_name=?";
        $result = $connection->query(
            $sql, array(
                $schemaName, $tableName
            )
        );
        if ($layerInfo = $result->fetch()) {
            if ( $layerInfo["type"]==="GEOMETRY" ){
                $sql = "SELECT distinct public.geometrytype(the_geom) as type from ".$schemaName.".".$tableName." limit 1";
                $result = $connection->query($sql);
                $layerInfo = $result->fetch();
            }
        }
        //convert type to geojson format
        switch($layerInfo['type']){
            case "POINT":
                return "Point";
                break;
            case "MULTIPOINT":
                return "MultiPoint";
                break;
            case "LINESTRING":
                return "LineString";
                break;
            case "MULTILINESTRING":
                return "MultiLineString";
                break;
            case "MULTIPOLYGON":
                return "MultiPolygon";
                break;
            case "POLYGON":
                return "Polygon";
                break;
            default:
                return $layerInfo["type"];
                break;    
        }
    }

    /**
     * find layers with metadataUuid
     * @param layers array of layers
     * @param tabMetadataUid results
     * @return nothing
     */
    protected function searchMetadataUuidInLayers($layers, &$tabMetadataUuid)
    {   
        foreach($layers as $key => $layer){
            if (array_key_exists('class', $layer) && strcmp($layer["class"], "LayerGroup") == 0) {
                if(array_key_exists("layers", $layer)){
                    $this->searchMetadataUuidInLayers($layer["layers"], $tabMetadataUuid);
                }
            }elseif(array_key_exists('extension', $layer) && array_key_exists('layerMetadataUuid', $layer["extension"])){
                $tabMetadataUuid[$layer["extension"]["layerMetadataUuid"]] = $layer["extension"]["layerName"];
            }
        }
        
    }
}
