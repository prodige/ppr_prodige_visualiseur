<?php

namespace Visualiseur\Prodige\Controllers\Traits;

include_once(BASE_PATH."/vendor/jasig/phpcas/source/CAS.php");

use Phalcon\Logger\Adapter\File;
use Visualiseur\Prodige\Models\User;
use Visualiseur\Prodige\Models\Map;
use Visualiseur\Prodige\Models\UserMap;
/**
 * UserTrait
 */
Trait UserTrait
{
  
    /**
     * Retourne l'identifiant de la personne connectée
     * @return string
     */
    protected function getCurentUserName()
    {
        if( !class_exists("\\phpCAS") ) {
          throw new \Exception("La librairie phpCAS n'est pas installée ou inaccessible.", 500);
        }
        
        $config = $this->getDi()->getConfig();
        
        $casVersion          = CAS_VERSION_3_0;
        $casDebug            = ( $config->path('params.cas_debug', false) ? true : false );
        $casHost             = $config->path('params.cas_host', 'localhost');
        $casContext          = $config->path('params.cas_context', '/');
        $casPort             = $config->path('params.cas_port', '443')+0;
        $casServerValidation = ( $config->path('params.server_validation', false) ? true : false );
        $casCertPath         = $config->path('params.server_ca_cert_path', '');
        $casCallBackUrl      = $config->path('params.cas_callback_url', '');
        $casProxyChain       = $config->path('params.cas_proxy_chain', false);
        $casProxy            = true;
   
        if( $casDebug ) {
           $config = $this->getDi()->getConfig();
           $logFile = $config->path('params.log_prod_path');
           //\phpCAS::setDebug($logFile);
        }
        
        if ( $casProxy ) {
            \phpCAS::proxy($casVersion, $casHost, $casPort, $casContext);
            if( !empty($casCallBackUrl) ) {
              \phpCAS::setFixedCallbackURL($this->config->params->PRODIGE_URL_CATALOGUE . $casCallBackUrl);
            }

            // Allow this client to be proxied
            if( $casProxyChain && is_array($casProxyChain) ) {
                \phpCAS::allowProxyChain(new \CAS_ProxyChain($casProxyChain));
            } else {
                \phpCAS::allowProxyChain(new \CAS_ProxyChain_Any());
            }
        } else {
            \phpCAS::client($casVersion, $casHost, $casPort, $casContext);
        }
        
        // For production use set the CA certificate that is the issuer of the cert
        // on the CAS server and uncomment the line below
        if ($casCertPath != '') {
          \phpCAS::setCasServerCACert($casCertPath);
        }

        // For quick testing you can disable SSL validation of the CAS server.
        // THIS SETTING IS NOT RECOMMENDED FOR PRODUCTION.
        // VALIDATING THE CAS SERVER IS CRUCIAL TO THE SECURITY OF THE CAS PROTOCOL!
        if( false === $casServerValidation ) {
          \phpCAS::setNoCasServerValidation();
        }
        
        \phpCAS::handleLogoutRequests(false);

        //force auth
        $authenticated = \phpCAS::checkAuthentication();
        /*if(strpos( $this->request->getURI(), "/carto/context")!==false){
            $authenticated = \phpCAS::checkAuthentication();
        }else{
            $authenticated = \phpCAS::isAuthenticated();            
        }*/
        
        if( is_bool($authenticated) && $authenticated === false ) {
          $userName = $config->path('params.PRO_USER_INTERNET_USR_ID', 'vinternet');
        } else {
          $userName = \phpCAS::getUser();
        }
        
        return $userName;
    }

    /**
     * Retreive a Proxy Granting Ticket for a specific service url
     * @param string $service the service to be proxied 
     * @return mixed the ticket, or false
     * @throws Exception
     */
    protected function getPGT($service)
    {
        $err_code = $err_msg = null;
        try {
            return \phpCAS::retrievePT($service, $err_code, $err_msg);
        } catch(\Exception $e) {
            throw $e;
        }
    }

    /**
     * Force la vérification de l'identifiant de connexion auprès du CAS
     * Si non connecté, la session pour DataCarto est modifiée pour tromper phpCAS afin de ne pas lancer de requête de vérification auprès du serveur CAS.
     * Retourne l'identifiant de connexion
     * @return string
     */
    protected function checkCurrentUser()
    {
        $config = $this->getDi()->getConfig();
        $anonymousName = $config->path('params.PRO_USER_INTERNET_USR_ID', 'vinternet');
       
      
        $userName = $this->getCurentUserName();
            
        if( $this->session->has('phpCAS') ) {
          $phpCAS = ( $this->session->has('phpCAS') ? $this->session->get('phpCAS') : array() );
          
          if( !isset($phpCAS["user"]) ) {
              $userName = $anonymousName;
              
              $phpCAS["user"] = $userName;
              $phpCAS['unauth_count'] = 0;
              $phpCAS['auth_checked'] = false;
              $this->session->set("phpCAS", $phpCAS);
          }
        }

        return $userName;
    }
    
    
    
    /**
     * Retourne l'entité de l'utilisateur connecté
     * @return User
     */
    protected function getCurrentUser()
    {
        $config = $this->getDi()->getConfig();
        $anonymousName = $config->path('params.PRO_USER_INTERNET_USR_ID', 'vinternet');

        if( $this->session->has('phpCAS') ) {
            $session = $this->session->get('phpCAS');
            if(isset($session["user"])){
                $userName = $session ["user"];
            }else{
                $userName = $this->getCurentUserName();
            }
        } else {
            $userName = $this->getCurentUserName();
    
        }

        $user = User::findFirst(["userName = :USERNAME:", "bind" => ['USERNAME' => $userName ]]);
        if( !is_object($user) ) {
            $user = User::findFirst(["userName = :USERNAME:", "bind" => ['USERNAME' => $anonymousName ]]);
        }
        if( !is_object($user) ) {
            throw new \Exception('Unable to find current user.', 500);
        }
        return $user;
    }
    
    /**
     * renvoit true si l'utilisateur n'est pas connecté
     * 
     * @return bool
     */
    protected function isUserInternet()
    {
        $config = $this->getDi()->getConfig();
        $currentUser =  $this->getCurrentUser();
        return ($currentUser->userName === $config->path('params.PRO_USER_INTERNET_USR_ID', 'vinternet'));
    }
    /**
     * Vérifie les accès de l'utilisateur à la carte
     * @param string $map  nom de la carte
     * 
     * @return bool
     */
    protected function isUserHasRightOnMap($map)
    {  
        
        $this->checkCurrentUser();
        //on first connection, or reloading map, remove session map
        if(strpos( $this->request->getURI(), "/carto/context")!==false && $this->session->has($map)){
            $this->session->remove($map) ;
        }

        if( $this->session->has($map) ){
            return $this->session->get($map) ; 
        } else {
            $curl = curl_init();
            if( !is_resource($curl) ) {
                throw new \Exception("Unable to init curl connexion : ", 500);
            }  
            //get uuid from mapfile
            $connection = new \Phalcon\Db\Adapter\Pdo\Postgresql(
                [
                    'host'      => $this->config->params->prodige_host,
                    'username'  => $this->config->params->prodige_user,
                    'port'      => $this->config->params->prodige_port,
                    'password'  => $this->config->params->prodige_password,
                    'dbname'   =>  $this->config->params->prodige_name
                ]
            );

            $sql = " SELECT map_wmsmetadata_uuid from carmen.map where map_file= ? and published_id is null limit 1;";

            $result = $connection->query(
                $sql,
                [
                    str_replace(".map", "", $map),
                ]
            );
            
            $uuids = $result->fetch();
            if(empty($uuids)){
                return false;
            }else{
                $uuid = $uuids["map_wmsmetadata_uuid"];
            }
      
            //call verif rights service with proxy ticket            
            $url = $this->config->params->PRODIGE_URL_CATALOGUE."/prodige/verify_rights";
            $url.= "?TRAITEMENTS=NAVIGATION";
            if( false === stripos($map, 'layers/') ) {
                $url.= "&OBJET_TYPE=service";
                $url.= "&OBJET_STYPE=invoke";
            } else {
                $url.= "&OBJET_TYPE=dataset";
            }
            
            $url.= "&uuid=".$uuid;

            $ticket = $this->getPGT($url);

            $url.="&ticket=".$ticket;
           
           
            $optEnabled =
                curl_setopt($curl, CURLOPT_URL, $url)
                && curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false)
                && curl_setopt($curl, CURLOPT_BINARYTRANSFER, true)
                && curl_setopt($curl, CURLOPT_RETURNTRANSFER, true)
                && curl_setopt($curl, CURLOPT_FRESH_CONNECT, false)
                && curl_setopt($curl, CURLOPT_HEADER, false)
                && curl_setopt($curl, CURLOPT_TIMEOUT, 30)
                ;
            
            if( !$optEnabled ) {
              throw new \Exception(sprintf("Unable to parameter curl connexion : [%s] %s", curl_errno($curl), curl_error($curl)), 500);
            }
    
            $resultJson = curl_exec($curl);
            $rights = json_decode($resultJson, true);
           
            if( is_resource($curl) ) {
                curl_close($curl);
            }
            
            if($rights  && isset($rights["NAVIGATION"]) && $rights["NAVIGATION"]){
                $this->session->set($map, true); 
            }else{
                $this->session->set($map, false); 
            }
            return $this->session->get($map) ;
        
        }
    }

    /**
     * Log access to map
     * @param contextObj objet contexte
     * 
     * @return bool
     */
    protected function addLog($contextObj){
        if(isset($contextObj["properties"]["title"])){
            $mapTitle = $contextObj["properties"]["title"];
        }
        
        if(isset($contextObj["properties"]["extension"]["MapMetadata"]["links"]["via"]["href"])){
            $mapTitle .= ";".basename($contextObj["properties"]["extension"]["MapMetadata"]["links"]["via"]["href"]);
        }

        $curl = curl_init();
        if( !is_resource($curl) ) {
            throw new \Exception("Unable to init curl connexion : ", 500);
        }  
        //call add log with proxy ticket            
        $url = $this->config->params->PRODIGE_URL_CATALOGUE."/prodige/add_log";
        $url.= "?logFile=ConsultationCarte";
        $url.= "&objectName=".urlencode($mapTitle);

        $ticket = $this->getPGT($url);

        $url.="&ticket=".$ticket;
       
       
        $optEnabled =
            curl_setopt($curl, CURLOPT_URL, $url)
            && curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false)
            && curl_setopt($curl, CURLOPT_BINARYTRANSFER, true)
            && curl_setopt($curl, CURLOPT_RETURNTRANSFER, true)
            && curl_setopt($curl, CURLOPT_FRESH_CONNECT, false)
            && curl_setopt($curl, CURLOPT_HEADER, false)
            && curl_setopt($curl, CURLOPT_TIMEOUT, 30)
            ;
        
        if( !$optEnabled ) {
          throw new \Exception(sprintf("Unable to parameter curl connexion : [%s] %s", curl_errno($curl), curl_error($curl)), 500);
        }
        $resultJson = curl_exec($curl);
        
        $log = json_decode($resultJson, true);
        
        if( is_resource($curl) ) {
            curl_close($curl);
        }
        if($log && isset($log["success"]) && $log["success"]){
            return true;
        }
        return false;
        
    }
}
