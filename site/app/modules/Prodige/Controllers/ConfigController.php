<?php

namespace Visualiseur\Prodige\Controllers;

use Visualiseur\Core\Controllers\Traits\HttpTrait;
use Visualiseur\Core\Controllers\Traits\RequestTrait;
use Visualiseur\Prodige\Controllers\Traits\UserTrait;


/**
 * Class ProdigeController
 *
 * @package Visualiseur\Prodige\Controllers
 */
class ConfigController extends ControllerBase
{
  
  use HttpTrait;
  use RequestTrait;
  use UserTrait;
  /**
   * config
   */
  public function indexAction()
  {
    
    //$this->checkCurrentUser();
    $config = $this->getDi()->getConfig();
    
    $casloginUrl  = "https://" . $config->path('params.cas_host', '') . ':' . 
                    $config->path('params.cas_port', '') .
                    $config->path('params.cas_context', '').
                    "/login?service=";

    $caslogoutUrl = "https://" . $config->path('params.cas_host', '') . ':' . 
                    $config->path('params.cas_port', '') .
                    $config->path('params.cas_context', '').
                    "/logout?service=";
    //force load user via phpCAS
    $this->checkCurrentUser();
    $currentUser =  $this->getCurrentUser();

    $data = array(
      "login" => $currentUser->userName,
      "name" => $currentUser->name,
      "firstName" => $currentUser->firstName,
      "email" => $currentUser->email,
      "userInternet" => ($currentUser->userName === $config->path('params.PRO_USER_INTERNET_USR_ID', 'vinternet')),
      "URLS" => [
        "admincarto" => $this->config->params->PRODIGE_URL_ADMINCARTO,
        "catalogue" => $this->config->params->PRODIGE_URL_CATALOGUE,
        "caslogin"   => $casloginUrl,
        "caslogout"  => $caslogoutUrl
      ]
       
    );

    //for jsonp request
    $callback = $this->request->getQuery('callback', null);    

    $this->sendJsonResponse($data, $callback);
    
    exit();
    
  }

  /**
   * check-user action
   */
  public function checkuserAction()
  {
    $data = array("userName" => $this->checkCurrentUser() );
    $this->sendJsonResponse($data);
    exit();
  }    

}
