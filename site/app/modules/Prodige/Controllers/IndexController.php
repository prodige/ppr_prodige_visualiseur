<?php

namespace Visualiseur\Prodige\Controllers;

/**
 * Class IndexController
 *
 * @package Visualiseur\Prodige\Controllers
 */
class IndexController extends ControllerBase
{
  /**
   * Index
   */
  public function indexAction()
  {
    die(__FILE__ . '<br/>' . __METHOD__ . ' ligne : ' . __LINE__);
  }
}
