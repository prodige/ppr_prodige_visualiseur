<?php

namespace Visualiseur\Prodige\Controllers;

use Visualiseur\Core\Controllers\Traits\HttpTrait;
use Visualiseur\Core\Controllers\Traits\RequestTrait;
use Visualiseur\Prodige\Controllers\Traits\UserTrait;

/**
 * Class CheckuserController
 *
 * @package Visualiseur\Prodige\Controllers
 */
class CheckuserController extends ControllerBase
{
    use HttpTrait;
    use RequestTrait;
    use UserTrait;

    /**
     * check-user action
     */
    public function indexAction()
    {
      $data = array("userName" => $this->checkCurrentUser() );
      
      $this->sendJsonResponse($data);
      exit();
    }    
    

}
