<?php

namespace Visualiseur\Prodige\Controllers;

use Visualiseur\Core\Controllers\Traits\HttpTrait;
use Visualiseur\Core\Controllers\Traits\RequestTrait;
use Visualiseur\Prodige\Controllers\Traits\UserTrait;
use Visualiseur\Prodige\Models\FavoriteArea;
use Visualiseur\Prodige\Models\User;

/**
 * Class FavoriteAreaController
 *
 * @package Visualiseur\Prodige\Controllers
 */
class FavoriteareaController extends ControllerBase
{

    use HttpTrait;
    use RequestTrait;
    use UserTrait;

    public function listAction()
    {
        $response = new \Phalcon\Http\Response();
        try {
            $checkUser = $this->checkCurrentUser();
            $userName = $this->getCurrentUser()->getUserName();
            $userId = $this->getCurrentUser()->getId();

            $favoriteAreas = FavoriteArea::find(["idUser = :IDUSER:", "bind" => ['IDUSER' => $userId]]);
            $result = array();

            foreach ($favoriteAreas as $favoriteArea) {
                $arrayFavoriteArea = json_decode($favoriteArea->favoriteArea, true);
                if (!is_null($arrayFavoriteArea) && array_key_exists("title", $arrayFavoriteArea)) {
                    array_push($result, array("id" => $favoriteArea->id, "title" => $arrayFavoriteArea["title"]));
                }
            }

            $titleSorterFunction = function($a, $b) {
                return strcmp(strtolower($a["title"]), strtolower($b["title"]));
            };
            usort($result, $titleSorterFunction);

            $response->setJsonContent([
                "success" => true,
                "userName" => $userName,
                "message" => "",
                "data" =>
                $result,
            ]);
            return $response;

        } catch (\Exception $e) {
//            throw $e; //for dev
            $response->setJsonContent([
                "success" => false,
                "message" => "Failed to list favorite area. " . $e->getMessage(),
            ]);
            return $response;
        }
    }

    public function saveAction()
    {
        $response = new \Phalcon\Http\Response();
        $userName = "";
        try {
            // if checkCurrentUser is uncommented, then request is returning as get so failing
            $checkUser = $this->checkCurrentUser();
            $userName = $this->getCurrentUser()->getUserName();
            $userId = $this->getCurrentUser()->getId();

            $anonymousName = $this->config->params->PRO_USER_INTERNET_USR_ID;

            if (!$this->request->isPost()) {
                $response->setJsonContent([
                    "success" => false,
                    "message" => "Post Request is mandatory.",
                ]);
                return $response;
            }

            if (strcmp($userName, $anonymousName) == 0) {
//                throw new \Exception("Failed to identified current user."); //for dev

                $response->setStatusCode(401);
                $response->setContent("Failed to identified current user.");
                $response->send();
                return $response;

            }

            $json = json_decode($this->request->getRawBody(), true);

            if (is_null($json)) {
                throw new \Exception("Failed to parse JSON. Content length is " . strlen($this->request->getRawBody()));
            }

            if (!isset($json["bbox"])) {
                throw new \Exception("parameter bbox is missing.");
            }
            if (!isset($json["title"])) {
                throw new \Exception("parameter title is missing.");
            }

            $favoriteArea = $json;
            $title = $json["title"];
            $jsonFavoriteArea = json_encode($favoriteArea);

            $newFavoriteArea = new FavoriteArea();
            $newFavoriteArea->idUser = $userId;
            $newFavoriteArea->favoriteArea = $jsonFavoriteArea;
            $newFavoriteArea->save();

            $response->setJsonContent([
                "success" => true,
                "userName" => $userName,
                "message" => "",
                "data" => [
                    "id" => $newFavoriteArea->id,
                    "title" => $title,
                    "favoriteArea" => $jsonFavoriteArea,
                ]
            ]);
            return $response;
        } catch (\Exception $e) {
//            throw $e; //for dev
            $response->setJsonContent([
                "success" => false,
                "userName" => $userName,
                "message" => "Failed to save favorite area. " . $e->getMessage(),
            ]);
            return $response;
        }
    }

    public function getAction()
    {
        $response = new \Phalcon\Http\Response();
        try {

            $favoriteAreaId = $this->request->getQuery("id");
            if (is_null($favoriteAreaId)) {
                throw new \Exception("id is missing.");
            }
            if (!is_numeric($favoriteAreaId)) {
                throw new \Exception("id is invalid.");
            }

            $checkUser = $this->checkCurrentUser();
            $userName = $this->getCurrentUser()->getUserName();
            $userId = $this->getCurrentUser()->getId();

            $favoriteAreas = FavoriteArea::find(["idUser = :IDUSER: and id = :IDFAVORITEAREA:", "bind" => ['IDUSER' => $userId, 'IDFAVORITEAREA' => $favoriteAreaId]]);
            $result = array();

            foreach ($favoriteAreas as $favoriteArea) {
                $arrayFavoriteArea = json_decode($favoriteArea->favoriteArea, true);

                if (!is_null($arrayFavoriteArea) && array_key_exists("title", $arrayFavoriteArea)) {

                    $response->setJsonContent([
                        "success" => true,
                        "userName" => $userName,
                        "message" => "",
                        "data" => $arrayFavoriteArea
                    ]);
                    return $response;
                }
            }

            throw new \Exception("favorite area not found.");
        } catch (\Exception $e) {
//            throw $e; //for dev
            $response->setJsonContent([
                "success" => false,
                "message" => "Failed to get favorite area. " . $e->getMessage(),
            ]);
            return $response;
        }
    }

    public function renameAction()
    {
        $response = new \Phalcon\Http\Response();
        try {

            $favoriteAreaId = $this->request->getQuery("id");
            if (is_null($favoriteAreaId)) {
                throw new \Exception("id is missing.");
            }
            if (!is_numeric($favoriteAreaId)) {
                throw new \Exception("id is invalid.");
            }
            $title = $this->request->getQuery("title");
            if (is_null($title)) {
                throw new \Exception("title is missing.");
            }

            $checkUser = $this->checkCurrentUser();
            $userName = $this->getCurrentUser()->getUserName();
            $userId = $this->getCurrentUser()->getId();

            $favoriteAreas = FavoriteArea::find(["idUser = :IDUSER: and id = :IDFAVORITEAREA:", "bind" => ['IDUSER' => $userId, 'IDFAVORITEAREA' => $favoriteAreaId]]);

            foreach ($favoriteAreas as $favoriteArea) {
                $arrayFavoriteArea = json_decode($favoriteArea->favoriteArea, true);
                if (!is_null($arrayFavoriteArea) && array_key_exists("title", $arrayFavoriteArea)) {
                    $arrayFavoriteArea["title"] = $title;
                    $jsonFavoriteArea = json_encode($arrayFavoriteArea);
                    $favoriteArea->favoriteArea = $jsonFavoriteArea;
                    $favoriteArea->save();

                    $response->setJsonContent([
                        "success" => true,
                        "message" => "",
                        "userName" => $userName,
                    ]);
                    return $response;
                }
            }

            throw new \Exception("favorite area not found.");
        } catch (\Exception $e) {
//            throw $e; //for dev
            $response->setJsonContent([
                "success" => false,
                "message" => "Failed to rename favorite area. " . $e->getMessage(),
            ]);
            return $response;
        }
    }

    public function deleteAction()
    {
        $response = new \Phalcon\Http\Response();
        try {

            $favoriteAreaId = $this->request->getQuery("id");
            if (is_null($favoriteAreaId)) {
                throw new \Exception("id is missing.");
            }
            if (!is_numeric($favoriteAreaId)) {
                throw new \Exception("id is invalid.");
            }

            $checkUser = $this->checkCurrentUser();
            $userName = $this->getCurrentUser()->getUserName();
            $userId = $this->getCurrentUser()->getId();

            $favoriteAreas = FavoriteArea::find(["idUser = :IDUSER: and id = :IDFAVORITEAREA:", "bind" => ['IDUSER' => $userId, 'IDFAVORITEAREA' => $favoriteAreaId]]);

            foreach ($favoriteAreas as $favoriteArea) {
                $favoriteArea->delete();

                $response->setJsonContent([
                    "success" => true,
                    "message" => "",
                    "userName" => $userName,
                ]);
                return $response;
            }

            throw new \Exception("favorite area not found.");
        } catch (\Exception $e) {
//            throw $e; //for dev
            $response->setJsonContent([
                "success" => false,
                "message" => "Failed to delete favorite area. " . $e->getMessage(),
            ]);
            return $response;
        }
    }

}
