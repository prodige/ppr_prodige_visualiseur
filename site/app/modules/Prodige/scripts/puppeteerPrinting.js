/**
 * Script permettant la génération PDF ou Image (PNG, JPG) d'un template du visualiseur Prodige.
 * 
 * #1 - On récupère d'abord les paramètres nécessaires à la bonne exécution :
 *  [0] -> L'URL du visualiseur lite'.
 *  [1] -> Le nom du fichier de contexte.
 *  [2] -> Le nom du fichier de template.
 *  [3] -> Le type d'impression (pdf || png).
 *  [4] -> Nom du fichier de sortie (avec chemin).
 *  [5] -> Format d'impression (Obligatoire pour PDF a3 ou a4).
 *  [6] -> Orientation de l'impression (Obligatoire pour PDF portrait ou landscape).
 * 
 * #2 - On appelle l'application VisualiseurLite avec la bonne URL.
 * #3 - A la fin de l'écriture du fichier de sortie, le processus s'arrête.
 */

const puppeteer = require('puppeteer');


// #1 - On récupère d'abord les paramètres nécessaires à la bonne exécution
if (
  process.argv.length >= 9
) {

  console.log(process);
  
  const visualiseurLiteUrl = process.argv[2];
  const contextFilePath = process.argv[3];
  const templateFilePath = process.argv[4];
  const printType = process.argv[5];
  let exportFilePathAndName = process.argv[6];

  let printFormat = null;
  let printOrientation = null;
  let title = null;
  let subtitle = null;
  if (process.argv.length >= 9) {
    printFormat = process.argv[7];
    printOrientation = process.argv[8];
  }

  console.log(printOrientation);

  // On vérifie que tous les paramètres sont présents (hors Nom du fichier de sortie).
  if (
    contextFilePath && 
    typeof contextFilePath === 'string' && 
    contextFilePath.endsWith('.geojson') && 

    templateFilePath && 
    typeof templateFilePath === 'string' &&
    templateFilePath.endsWith('.tpl') && 

    ( 
      printType === 'pdf' &&
      printFormat &&
      (
        printFormat === 'A3' ||
        printFormat === 'A4'
      ) &&
      printOrientation &&
      (
        printOrientation === 'portrait' ||
        printOrientation === 'landscape'
      )
    ) ||
    printType === 'png' ||
    printType === 'jpg' 
  ) {  
    (async () => {     
      try {
        // On créer maintenant le navigateur web avec puppeteer.
        const browser = await puppeteer.launch({
          ignoreHTTPSErrors: true,
          args: ['--no-sandbox', '--disable-web-security'],
        });

        // On ouvre la page web vide.
        const page = await browser.newPage();

        // #2 - On appelle l'application VisualiseurLite avec la bonne URL.
        const contextMapFilePath = contextFilePath.split('.geojson')[0] + '.tmp';
        console.log(visualiseurLiteUrl + '?contextPath=' + contextMapFilePath + '&templatePath=' + templateFilePath);
        await page.goto(
          visualiseurLiteUrl + '?contextPath=' + contextMapFilePath + '&templatePath=' + templateFilePath, 
          { waitUntil: 'networkidle0', timeout: 500000 }
        );

        const title = await page.evaluate(() => document.querySelector('#title') ? document.querySelector('#title').textContent : null);
        const subtitle = await page.evaluate(() => document.querySelector('#subtitle') ? document.querySelector('#subtitle').textContent : null);

        await page.emulateMedia('screen');

        // On imprime la page selon le paramètre donné dans la commande.
        if (printType == 'pdf') {
          await page.pdf({
            path: exportFilePathAndName,
            /* headerTemplate: '<div id="header" style="display: block; width: 100%; font-size: 10px !important; overflow-wrap: break-word; padding: 0 !important; margin-left: 25px !important; margin-right: 25px !important;">'
                + (title ? '<h3 style="margin: auto;">' + title + '</h3>' : '')
                + (subtitle ? '<span style="margin: auto;">' + subtitle + '</span>' : '')
              + '</div>',
            footerTemplate: '<div id="footer"></div>', */
            displayHeaderFooter: false,
            landscape: (printOrientation === 'portrait' ? false : true),
            format: printFormat,
            printBackground: true,
            margin: { left: '1cm', top: '1cm', right: '1cm', bottom: '1cm' }
          });
        } else {
          await page.setViewport({
            width: 900,
            height: 900
          });
          await page.screenshot({path: exportFilePathAndName});
        }

        // #3 - A la fin de l'écriture du fichier de sortie, le processus s'arrête.
        await browser.close();

        // Si on arrive ici , c'est que tout est OK.
        process.exit(0);

      } catch (ErrorPuppeteer) {
        console.log("ErrorPuppeteer - ", ErrorPuppeteer); 
        process.exit(1);
      }
      
    })();
  } else {
    // Les paramètres passés semblent invalides.
    console.log(new Error('Given parameters looks invalid.')); 
    process.exit(1);
  }
} else {
  // Il manque des paramètres au processus.
  console.log(new Error('Some parameters are missing. Needed : \n[0] -> Context filename.\n[1] -> Template filename.\n[2] -> Print type (pdf || png || jpg).\n[3] -> Output filename (with path).\n[4] -> (Only for pdf) Print format (a4 || a3).\n[5] -> (Only for pdf) Print orientation (portrait || landscape).\n'));
  process.exit(1);
}
