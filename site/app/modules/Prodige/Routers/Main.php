<?php

namespace Visualiseur\Prodige\Routers;

use Phalcon\Mvc\Router\Group;

/**
 * Class Main
 * Basic router
 *
 * @package Visualiseur\Prodige\Routers
 */
class Main extends Group
{
  /**
   * Home page
   */
  const HOME = 'prodigeMainHome';
  /**
   * Routes with only controllers
   */
  const CONTROLLER = 'prodigeMainController';
  /**
   * Routes with action and/or params
   */
  const ACTION = 'prodigeMainAction';

  /**
   * Main router definitions
   */
  public function initialize()
  {
    $this->setPrefix('/carto/');
    $this->add('')->setName(self::HOME);

    $this->add(
      ':controller',
      ['controller' => 1]
    )->setName(self::CONTROLLER);

    $this->add(
      ':controller/:action/:params',
      [
        'controller' => 1,
        'action'     => 2,
        'params'     => 3,
      ]
    )->setName(self::ACTION);
  }
}
