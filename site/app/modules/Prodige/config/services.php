<?php

use Visualiseur\Core\Plugins\ParamsToArray;

/** @var Phalcon\Di $di */
$di->setShared('dispatcher', function () use ($di) {
    $dispatcher = new \Phalcon\Mvc\Dispatcher();
    $dispatcher->setEventsManager($di->getShared('eventsManager'));
    $dispatcher->setDefaultNamespace('Visualiseur\Prodige\Controllers');
    $dispatcher->getEventsManager()->attach(
        "dispatch:beforeDispatchLoop",
        new ParamsToArray()
    );
    return $dispatcher;
});

$di->setShared('view', function () {
    $view = new \Phalcon\Mvc\View();
    // $view->setViewsDir(\Visualiseur\Prodige\Module::PATH . '/Views/Default/scripts/')
    //     ->registerEngines([
    //         '.volt' => 'voltService',
    //     ]);
    $view->setViewsDir(\Visualiseur\Prodige\Module::PATH . '/Views/');
    return $view;
});