<?php

return [
    'application' => [
        'controllersDir' => \Visualiseur\Prodige\Module::PATH . '/Controllers/',
        // 'viewsDir'       => \Visualiseur\Prodige\Module::PATH . '/Views/Default/scripts/',
        'viewsDir'       => \Visualiseur\Prodige\Module::PATH . '/Views/',
    ],
];