<?php

namespace Visualiseur\Prodige\Models;

use Phalcon\Mvc\Model;

/**
 * Map
 * catalogue.stockage_carte
 */
class Map extends Model
{
    public $id;
    public $ts;
    public $stkcardId;
    public $pathName;
    public $stkServer;

    /**
     * Columns mapping
     * @return array
     */
    public function columnMap()
    {
        return [
            'pk_stockage_carte' => 'id',
            'ts'                => 'ts',
            'stkcard_id'        => 'stkcardId',
            'stkcard_path'      => 'pathName',
            'stk_server'        => 'stkServer',
        ];
    }
    
    /**
     * Association mapping
     */
    public function initialize() 
    {
        $this->setSchema('catalogue');
        $this->setSource('stockage_carte');
        $this->hasMany('id', 'Visualiseur\Prodige\Models\UserMap', 'idMap');
    }

    /**
     * get id
     * @return int
     */
    public function getId()
    {
      return $this->id;
    }    
    
    /**
     * get full name
     * @return string
     */
    public function getPathName()
    {
      return $this->pathName;
    }
    
}
