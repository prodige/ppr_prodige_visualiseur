<?php

namespace Visualiseur\Prodige\Models;

use Phalcon\Mvc\Model;

class Context extends Model
{

    public $id;
    public $idUser;
    public $context;

    /**
     * Columns mapping
     * @return array
     */
    public function columnMap()
    {
        return [
            'id' => 'id',
            'fk_utilisateur' => 'idUser',
            'context' => 'context',
        ];
    }

    /**
     * Association mapping
     */
    public function initialize()
    {
        $this->setSchema('catalogue');
        $this->setSource('utilisateur_contexte');
        $this->belongsTo('idUser', 'Visualiseur\Prodige\Models\User', 'id', array('foreignKey' => TRUE));
    }

    public function getSequenceName()
    {
        return 'catalogue.seq_utilisateur_contexte';
    }

}
