<?php

namespace Visualiseur\Prodige\Models;

use Phalcon\Mvc\Model;

/**
 * User
 * catalogue.utilisateur
 */
class User extends Model
{
    public $id;
    public $userName;
    public $name;
    public $firstName;
    public $email;

    /**
     * Columns mapping
     * @return array
     */
    public function columnMap()
    {
        return [
            'pk_utilisateur' => 'id',
            'usr_id'         => 'userName',
            'usr_nom'        => 'name',
            'usr_prenom'     => 'firstName',
            'usr_email'      => 'email',
        ];
    }

    /**
     * Association mapping
     */
    public function initialize() 
    {
        $this->setSchema('catalogue');
        $this->setSource('v_utilisateur');
        $this->hasMany('id', 'Visualiseur\Prodige\Models\UserMap', 'idUser');
        $this->hasMany('id', 'Visualiseur\Prodige\Models\Context', 'idUser');
        $this->hasMany('id', 'Visualiseur\Prodige\Models\FavoriteArea', 'idUser');
    }
    
    /**
     * get id
     * @return int
     */
    public function getId()
    {
      return $this->id;
    }
    
    /**
     * get full name
     * @return string
     */
    public function getfullName()
    {
      return trim($this->firstName." ".$this->name);
    }
    
    /**
     * get name
     * @return string
     */
    public function getName()
    {
      return $this->name;
    }

    /**
     * get firstName
     * @return string
     */
    public function getFirstName()
    {
      return $this->firstName;
    }

    /**
     * get userName
     * @return string
     */
    public function getUserName()
    {
      return $this->userName;
    }

    /**
     * get email
     * @return string
     */
    public function getEmail()
    {
      return $this->email;
    }


}



