<?php

namespace Visualiseur\Prodige\Models;

use Phalcon\Mvc\Model;

class FavoriteArea extends Model
{

    public $id;
    public $idUser;
    public $favoriteArea;

    /**
     * Columns mapping
     * @return array
     */
    public function columnMap()
    {
        return [
            'id' => 'id',
            'fk_utilisateur' => 'idUser',
            'zone_favorite' => 'favoriteArea',
        ];
    }

    /**
     * Association mapping
     */
    public function initialize()
    {
        $this->setSchema('catalogue');
        $this->setSource('utilisateur_zone_favorite');
        $this->belongsTo('idUser', 'Visualiseur\Prodige\Models\User', 'id', array('foreignKey' => TRUE));
    }

    public function getSequenceName()
    {
        return 'catalogue.seq_utilisateur_zone_favorite';
    }

}
