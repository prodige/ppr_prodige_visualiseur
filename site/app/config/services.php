<?php

use Phalcon\Session\Adapter\Stream as SessionAdapter;
use Phalcon\Session\Manager as SessionManager;

/**
 * Shared configuration service
 */
/** @var Phalcon\Di $di */
$di->setShared('router', function () {
    $config = $this->getShared('config');
    $router = new \Phalcon\Mvc\Router(false);
    $router->removeExtraSlashes(true)
        ->setDefaultModule($config->application->moduleDefault);
    // mount module routes

    
    foreach ($config->application->modules as $module) {
        
        if (!is_file($module->path)) {
            continue;
        }
        $routers = require_once  dirname ($module->path) . '/config/routers.php';
        if (!empty($routers) && is_array($routers)) {
            foreach ($routers as $route) {
                $router->mount(new $route());
            }
        }
    }
    return $router;
});

$di->setShared('config', function () {
    return include APP_PATH . "/config/config.php";
});

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->setShared('url', function () {
    $config = $this->getShared('config');
    $url = new \Phalcon\Url();
    if (isset($config->application->baseUri)) {
        $url->setBaseUri($config->application->baseUri);
    }
    return $url;
});
$di->setShared('voltService', function ($view) {
    $volt = new \Phalcon\Mvc\View\Engine\Volt($view, $this);
    $directory = $this->getShared('config')->application->cacheDir . '/volt/';
    if (!is_dir($directory)) {
        mkdir($directory, 0755, true);
    }
    $volt->setOptions([
        "compiledPath"      => $directory,
        "compiledSeparator" => "-",
        "compileAlways"     => true,
    ]);
    return $volt;
});
/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->setShared('db', function () {
    $config = $this->getShared('config');
    $class = 'Phalcon\Db\Adapter\Pdo\\' . $config->database->adapter;
    $params = [
        'host'     => $config->database->host,
        'username' => $config->database->username,
        'password' => $config->database->password,
        'dbname'   => $config->database->dbname,
        'charset'  => $config->database->charset,
    ];
    if ($config->database->adapter == 'Postgresql') {
        unset($params['charset']);
    }
    $connection = new $class($params);
    return $connection;
});
/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->setShared('modelsMetadata', function () {
    return new \Phalcon\Mvc\Model\Metadata\Memory();
});
/**
 * Register the session flash service with the Twitter Bootstrap classes
 */
$di->setShared('flash', function () {
    return new \Phalcon\Flash\Direct([
        'error'   => 'alert alert-danger',
        'success' => 'alert alert-success',
        'notice'  => 'alert alert-info',
        'warning' => 'alert alert-warning',
    ]);
});
/**
 * Start the session the first time some component request the session service
 */
$di->setShared('session', function () {
    $session = new SessionManager();
    $files = new SessionAdapter([
        'savePath' => sys_get_temp_dir(),
    ]);
    $session->setAdapter($files);
    $session->start();

    return $session;
});

$di->setShared('view', function () {
    $config = $this->getConfig();

    $view = new View();
    $view->setDI($this);
    $view->setViewsDir($config->application->viewsDir);

    $view->registerEngines([
        '.volt' => function ($view) {
            $config = $this->getConfig();

            $volt = new VoltEngine($view, $this);

            $volt->setOptions([
                'compiledPath' => $config->application->cacheDir,
                'compiledSeparator' => '_',
                // force le calcul du cache pour le dev, � mettre en commentaire en production
                'compileAlways' => true  
            ]);

            return $volt;
        },
        '.phtml' => PhpEngine::class,
        '.tpl' => function($view, $di) {

            $smarty = new \Smarty($view, $di);
    
            $smarty->setTemplateDir($view->getViewsDir());
            $smarty->setCompileDir( '../app/viewscompiled' );
            $smarty->error_reporting = error_reporting() ^ E_NOTICE;
            $smarty->escape_html = true;
            $smarty->_file_perms = 0666;
            $smarty->_dir_perms = 0777;
            $smarty->force_compile = false;
            $smarty->compile_check = true;
            $smarty->caching = false;
            $smarty->debugging = true;
    
            return $smarty;
        }
    ]);

    return $view;
});