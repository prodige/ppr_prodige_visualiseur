<?php

/*
 * Modified: prepend directory path of current file, because of this file own different ENV under between Apache and command line.
 * NOTE: please remove this comment.
 */
defined('BASE_PATH') || define('BASE_PATH', getenv('BASE_PATH') ?: realpath(dirname(__FILE__) . '/../..'));
defined('APP_PATH') || define('APP_PATH', BASE_PATH . '/app');

// Cache the data for 2 days
/*$frontCache = new \Phalcon\Cache\Frontend\Data(["lifetime" => 86400, ]);
// Create the Cache setting memcached connection options
$cache = new \Phalcon\Cache\Backend\Memcache($frontCache, ["host" => "visualiseur-memcached", "port" => 11211, "persistent" => false, ]);

$configCacheName = "frontCartoConfig";
$cache->delete($configCacheName);
if( $cache->exists($configCacheName) ) {
  $prodigeConfig = $cache->get($configCacheName);
} else {*/
  $_fnEvalConfig = function(array $config) {
    foreach($config as $key => $value) {
      if(is_array($value)) {
        $config[$key] = $value;
      } else {
        $config[$key] = preg_replace_callback(
          "/\%([^\%]+)\%/", 
          function($matches) use ($config) {
            return (isset($matches[1]) && isset($config[$matches[1]]) ? $config[$matches[1]] : $matches[0]);
          },
          $value
        );
      }
    }
    return $config;
  };  
  $prodigeParametersYaml = new \Phalcon\Config\Adapter\Yaml(BASE_PATH.'/global_parameters.yaml');
  $prodigeConfigSrc = $prodigeParametersYaml->toArray()["parameters"];
  //for local config
  if(file_exists(BASE_PATH.'/dns_config.yml')){
    $prodigeConfigDNSYaml  = new \Phalcon\Config\Adapter\Yaml(BASE_PATH.'/dns_config.yml');
    $prodigeConfigDNS = $prodigeConfigDNSYaml["parameters"]->toArray();

    $prodigeConfigSrc = array_merge($prodigeConfigDNS, $prodigeConfigSrc);
  }
  $prodigeConfig = $_fnEvalConfig($prodigeConfigSrc);

  //core parameters
  $prodigeConfig ["PATH_DATA"] = $prodigeConfig["PRODIGE_PATH_DATA"];
  $prodigeConfig ["MAPIMAGE_DIR"] = "mapimage";
  $prodigeConfig ["URL_FRONTCARTO"] = $prodigeConfig["PRODIGE_URL_FRONTCARTO"];
  $prodigeConfig ["URL_ADMINCARTO"] = $prodigeConfig["PRODIGE_URL_ADMINCARTO"];
  $prodigeConfig ["URL_DATACARTO"] = $prodigeConfig["PRODIGE_URL_DATACARTO"];
  $prodigeConfig ["URL_TELECARTO"] = $prodigeConfig["PRODIGE_URL_TELECARTO"];
  $prodigeConfig ["URL_PRINT"] = $prodigeConfig["PRODIGE_URL_FRONTCARTO_PRINTER"];
  $prodigeConfig ["ACCOUNT_ID_IN_WXS"] = false;

//  $cache->save($configCacheName, $prodigeConfig);
//}


return new \Phalcon\Config([
    'debug'       => true,
    'database' => [
      'adapter'     => 'Postgresql',
      'host'        => $prodigeConfig["catalogue_host"],
      'port'        => $prodigeConfig["catalogue_port"],
      'username'    => $prodigeConfig["catalogue_user"],
      'password'    => $prodigeConfig["catalogue_password"],
      'dbname'      => $prodigeConfig["catalogue_name"],
      'charset'     => $prodigeConfig["catalogue_charset"],
  ],
	'application' => [
        'appDir'        => APP_PATH . '/',
        'libraryDir'    => APP_PATH . '/library/',
        'cacheDir'      => APP_PATH . '/../cache/',
        'modules'       => [
            'Prodige' => [
                'className' => Visualiseur\Prodige\Module::class,
                'path'      => APP_PATH . '/modules/Prodige/Module.php',
            ],
			'Core' => [
              'className' => Visualiseur\Core\Module::class,
              'path'      => APP_PATH . '/../vendor/alkante/bdl_visualiseur_core/Module.php',
            ]
        ],
        'moduleDefault' => 'Prodige',
        'namespaces'    => [
          'Visualiseur\Core'  => APP_PATH . '/../vendor/alkante/bdl_visualiseur_core/',
          'Visualiseur\Prodige' => APP_PATH . '/modules/Prodige/'
        ],
        'dirs' => [
          APP_PATH . '/../vendor/alkante/bdl_visualiseur_core/',
          APP_PATH . '/modules/Prodige/',
        ]
    ],
    'orm' => [
        'castOnHydrate'      => true,
        'notNullValidations' => false,
    ],
    'params'  => $prodigeConfig
]);
