<?php
/** @var Phalcon\Config $config */
require_once __DIR__."/../../vendor/autoload.php";

$loader = new \Phalcon\Loader();
$loader->registerDirs($config->application->dirs->toArray());
$loader->registerNamespaces($config->application->namespaces->toArray());
$loader->register();
