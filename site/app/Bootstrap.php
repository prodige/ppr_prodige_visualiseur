<?php

namespace App;

use Phalcon\Debug;
use Phalcon\Config;
use Phalcon\Mvc\Model;
use Phalcon\DI\FactoryDefault;
use Phalcon\Mvc\Application;

/**
 * Class Bootstrap
 */
class Bootstrap extends Application
{
  /**
   * Bootstrap constructor.
   */
  public function __construct()
  {
    // init DI and config service
    $di = new FactoryDefault();
    $this->setDI($di);

    include APP_PATH . '/config/services.php';
    $config = $this->getDI()->getConfig();

    // ORM setup
    Model::setup($config->orm->toArray());

    // register services
    $this->registerServices();

    // register modules
    $this->registerModules($config->application->modules->toArray());
    $this->setDefaultModule($config->application->moduleDefault);
  }

  /**
   * Services registry
   */
  private function registerServices()
  {
    $di = $this->getDI();
    $config = $this->getDI()->getShared('config');

    // custom config by environment?
    if (is_file(APP_PATH . '/config/config.' . APP_ENV . '.php')) {
      $config->merge(new Config(require_once APP_PATH . '/config/config.' . APP_ENV . '.php'));
    }

    // debug mode?
    if ($config->get('debug', false) === true) {
      error_reporting(E_ALL);
      ini_set('display_errors', 1);
      (new Debug())->listen(true, true);
    }

    require_once APP_PATH . '/config/loader.php';
    require_once APP_PATH . '/config/services.php';
  }

  /**
   * Init
   * @return string
   */
  //public function init(): string // with php 7.0 : TypeError: Return value of App\Bootstrap::init() must be of the type string, null returned - $response->setStatusCode(401);
  //public function init(): ?string // php 7.1 syntax - but cannot be tested at the moment
  public function init()
  {
    mb_internal_encoding('UTF-8');
    mb_http_output('UTF-8');

    return $this->handle($_SERVER['REQUEST_URI'])->getContent();
  }
}
