<?php
declare(strict_types=1);

use App\Bootstrap;
use Phalcon\Http\Response;
use Phalcon\Di\FactoryDefault;

error_reporting(E_ALL);

defined('PUBLIC_PATH') || define('PUBLIC_PATH', getenv('PUBLIC_PATH') ?: realpath(__DIR__));
defined('APP_PATH') || define('APP_PATH', realpath('../app'));
defined('APP_ENV') || define('APP_ENV', getenv('APP_ENV') ?: 'development');

require_once APP_PATH . '/../vendor/autoload.php';
require_once APP_PATH . '/Bootstrap.php';

try {
    echo (new Bootstrap())->init();
} catch (\Exception $e) {
    /*echo $e->getMessage() . '<br>';
    echo '<pre>' . $e->getTraceAsString() . '</pre>';*/
    $response = new Response();
    $response->setStatusCode(500, 'Internal Server Error');
    // $response->setContent($e->getMessage());  //for dev
    // Send response to the client
    $response->send();

}
