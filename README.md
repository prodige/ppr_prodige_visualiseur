# Module PRODIGE API du visualiseur

Module API du visualiseur PRODIGE (technologie PHP/Phalcon) permettant la lecture de contextes de cartes et offrant un ensemble de services web réalisés côté serveur (Exports, Sauvegarde de contexte,...)

### configuration

Modifier le fichier global_parameters.yml

### Installation

[documentation d'insatllation en développement](cicd/dev/README.md).

## Accès à l'application

- pas d'accès direct
