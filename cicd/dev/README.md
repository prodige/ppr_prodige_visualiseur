# README
<!-- TOC -->

- [README](#readme)
  - [Overview](#overview)
  - [Start development](#start-development)
  - [Stop development](#stop-development)
  - [General Docker commands](#general-docker-commands)
  - [Advanced Docker commands](#advanced-docker-commands)
    - [Clean commands](#clean-commands)
    - [Monitoring commands](#monitoring-commands)

<!-- /TOC -->

## Overview

This file provides commands to start development with Docker.

Requires:
 - Docker installed.
 - Docker well configured, cf. **/etc/docker/deamon.json**
 - User in `docker` group to avoid sudo/su right

## Start development

Issue these commands in this directory: `cd ./cicd/dev`

Run container
```bash
# Login on Alkante docker registry
docker login docker.alkante.com

# Put UID and GID in env file
echo -e "LOCAL_USER_ID=$(id -u)\nLOCAL_GROUP_ID=$(id -g)" > docker-compose.env

# Build dev image
docker-compose build

# Run containers
docker-compose up
```

In an other terminal, connect as www-data to the dev container
```bash
# Connect to container composer install as www-data
docker exec --user www-data -w /var/www/html/site -it ppr_prodige_visualiseur_dev_web bash
```

In container
```bash
# Install with composer
composer install

# Follow symfony logs
tail -f var/logs/dev.log
tail -f var/logs/prod.log
```

The url web site are:
 - site: http://localhost:4200

## Stop development

Issue these commands in this directory: `cd ./cicd/dev`

```bash
# Shutdown containers
docker-compose down

# Remove dev image
docker rmi ppr_prodige_visualiseur_dev_web
docker rmi ppr_prodige_visualiseur_dev_db
```

## General Docker commands

| Description                    | Commandes |
| ------------------------------ | ------------------------- |
| Display images            | `docker images` |
| Remove image              | `docker rmi myimage` |
| ------------------------------ | ------------------------- |
| Display all running containers | ```docker ps``` |
| Display all containers    | ```docker ps -a``` |
| Remove container          | ```docker rm mycontainer``` |
| Display info container    | ```docker inpect mycontainer``` |
| Display container output       | ```docker logs mycontainer``` |
| Stop container            | ```docker stop mycontainer``` |
| Kill container            | ```docker kill mycontainer``` |
| ------------------------------ | ------------------------- |
| Connection as root | ```docker exec --user 0 -w / -it ppr_prodige_visualiseur_dev_web bash``` |
| Connection as www-data | ```docker exec --user www-data -w /var/www/html -it ppr_prodige_visualiseur_dev_web bash``` |
| Connection as postgres | ```docker exec --user postgres -it ppr_prodige_visualiseur_dev_db bash``` |
| Run composer install | ```docker exec --user www-data -w /var/www/html -it ppr_prodige_visualiseur_dev_web composer install ``` |

## Advanced Docker commands


### Clean commands
```bash
# Stop all container
docker stop $(docker ps -a -q)

# Remove all container
docker rm $(docker ps -a -q)

# Delete all image with name or tag as "<none>"
docker rmi `docker images| egrep "<none>" |awk '{print $3}'`
```

### Monitoring commands

```bash
# Watch all container
watch -n 1 "docker ps -a"

# Watch all images
watch -n 1 "docker images"

# Watch all resources container
docker stats
```
