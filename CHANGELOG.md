# Changelog

All notable changes to [ppr_prodige_visualiseur](https://gitlab.adullact.net/prodige/ppr_prodige_visualiseur) project will be documented in this file.

## [4.4.11](https://gitlab.adullact.net/prodige/ppr_prodige_visualiseur/compare/4.4.10...4.4.11) - 2024-05-07

## [4.4.10](https://gitlab.adullact.net/prodige/ppr_prodige_visualiseur/compare/4.4.9...4.4.10) - 2023-10-18

## [4.4.9](https://gitlab.adullact.net/prodige/ppr_prodige_visualiseur/compare/4.4.8...4.4.9) - 2023-09-21

## [4.4.8](https://gitlab.adullact.net/prodige/ppr_prodige_visualiseur/compare/4.4.7...4.4.8) - 2023-09-21

## [4.4.7](https://gitlab.adullact.net/prodige/ppr_prodige_visualiseur/compare/4.4.6...4.4.7) - 2023-09-21

## [4.4.6](https://gitlab.adullact.net/prodige/ppr_prodige_visualiseur/compare/5.0.6-d...4.4.6) - 2023-09-19

## [5.0.6-d](https://gitlab.adullact.net/prodige/ppr_prodige_visualiseur/compare/4.4.5...5.0.6-d) - 2023-09-13

## [4.4.5](https://gitlab.adullact.net/prodige/ppr_prodige_visualiseur/compare/4.4.4...4.4.5) - 2023-08-22

## [4.4.4](https://gitlab.adullact.net/prodige/ppr_prodige_visualiseur/compare/4.4.3...4.4.4) - 2023-08-22

## [4.4.3](https://gitlab.adullact.net/prodige/ppr_prodige_visualiseur/compare/4.4.2...4.4.3) - 2023-06-23

## [4.4.2](https://gitlab.adullact.net/prodige/ppr_prodige_visualiseur/compare/4.4.1...4.4.2) - 2023-04-14

## [4.4.1](https://gitlab.adullact.net/prodige/ppr_prodige_visualiseur/compare/4.4.0...4.4.1) - 2023-04-06

## [4.4.0](https://gitlab.adullact.net/prodige/ppr_prodige_visualiseur/compare/4.4.0_rc13...4.4.0) - 2023-03-30

## [4.4.0_rc13](https://gitlab.adullact.net/prodige/ppr_prodige_visualiseur/compare/4.4.0_rc12...4.4.0_rc13) - 2022-12-08

## [4.4.0_rc12](https://gitlab.adullact.net/prodige/ppr_prodige_visualiseur/compare/4.4.0_rc11...4.4.0_rc12) - 2022-12-08

## [4.4.0_rc11](https://gitlab.adullact.net/prodige/ppr_prodige_visualiseur/compare/4.4.0_rc10...4.4.0_rc11) - 2022-12-08

